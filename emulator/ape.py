#!/usr/bin/python3

"""
What we need implemented:
- We need to have some sort of a "working" AutoPi board with an STN chip. It should have most of the requirements
  implemented so that we can use it for the rest of the tasks.
- We need some kind of a front-end, UI, that will show the work of the AutoPi Simulator. (flask, django, whichever is easier)
- We need the simulator to have a very basic example of automated testing.
"""

class STNEmulator(object):
    """
    The virtualized STN chip. Will act as the chip and maybe there should be another layer
    below this - which can be the vehicle itself. Or in some way to be able to define how the
    vehicle will respond to this.

    I think we should go with the vehicle thouhg, because that will make it easy to just simulate
    different options, just like the OBD-II simulator we have at work.

    Maybe the STN is like the relay between the device and the vehicle. It will send back all messages
    that are comming from the vehicle and will send to the vehicle all the messages from the device.
    That means that we don't need too much implementation here apart from write and read and some
    logic that will define how the two will interact based on the DS.
    """
    def __init__(self):
        pass

    def write(self):
        pass

    def read(self):
        pass
    

class PIDVehicle(object):
    """
    Vehicle that uses PIDs
    """
    pass


class CANVehicle(object):
    """
    Vehicle that uses CAN Messages (maybe skip this one)
    """
    pass


class SerialConnection(object):
    """
    An interface class that is used to communicate between the device and the STN chip (which uses serial)
    """
    pass