import time
import os
import subprocess

class SerialDeviceInterface(object):
    """
    setup in /tmp/emulator/ttyone
             /tmp/emulator/ttytwo
    """

    def __init__(self, device_one="device_one", device_two="device_two"):
        self.proc = None
        self.device_one = "/tmp/emulator/{}".format(device_one)
        self.device_two = "/tmp/emulator/{}".format(device_two)

        self.cmd = [
            "socat",
            "pty,rawer,link={},echo=0,b9600".format(self.device_one),
            "pty,rawer,link={},echo=0,b9600".format(self.device_two),
        ]

    def open(self):
        # create tmp directory
        os.makedirs('/tmp/emulator', exist_ok=True) # no need to throw if it exists already

        # start process
        self.proc = subprocess.Popen(self.cmd)
        # give time to start up
        time.sleep(.5)

    def close(self):
        self.proc.terminate()
        self.proc.wait()
        print(self.proc.poll())

    def read_one(self):
        # read one
        # do we need this?
        pass

    def read_two(self):
        # read two
        # do we need this?
        pass


if __name__ == "__main__":
    sdi = SerialDeviceInterface()
    sdi.open()
    time.sleep(1)
    sdi.close()
