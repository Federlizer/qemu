import re
import serial
import threading
import time

ELM_VERSION        = "ELM327 v1.4b"
STN_VERSION        = "STN1100v1.2.3"
STN_DEVICE_VERSION = "OBDLink r1.7"

class STN(threading.Thread):
    def __init__(self, device_path, vehicle):
        threading.Thread.__init__(self)

        # public fields
        self.lock = threading.Lock()

        # private fields
        self._sub_id = 0
        self._subs = [] # each element is a tuple -> (sub_id, function)
        self._communication_history = bytearray()
        self._vehicle = vehicle
        self._device_path = device_path
        self._ser = serial.Serial(self._device_path)
        self._reading = False

        self._AT_commands_map = {
            "I": self.at_version,
        }

        self._ST_commands_map = {
            "I": self.st_version,
            "DI": self.st_device_version,
        }

    def run(self):
        self._reading = True

        while self._reading:
            if self._ser.in_waiting > 0:
                b = self._ser.read(self._ser.in_waiting)
                self._communication_history.extend(b)
                line = b[:-1].decode('ascii').upper()
                print('Received {}'.format(line))

                if line.startswith('AT'):
                    # AT command
                    at_command = line[2:]
                    func = self._AT_commands_map.get(at_command, None)

                    if not func:
                        res = "?\n"
                    else:
                        res = "{}\n".format(func())

                elif line.startswith('ST'):
                    # ST command
                    st_command = line[2:]
                    func = self._ST_commands_map.get(st_command, None)

                    if not func:
                        res = "?\n"
                    else:
                        res = "{}\n".format(func())

                else:
                    # vehicle command
                    res = "{}\n".format(self._vehicle.pid_query(line))

                self._ser.write(bytes(res, 'ascii'))
                self._communication_history.extend(bytearray(res, 'ascii'))

                new_communication_history = self.get_communication_history()

                for sub in self._subs:
                    sub[1](new_communication_history)

            time.sleep(.5)

    def stop_reading(self):
        self._reading = False

    def subscribe(self, func):
        self._sub_id += 1
        sub_id = self._sub_id
        self._subs.append( (sub_id, func) )
        return sub_id

    def unsubscribe(self, sub_id):
        print(self._subs)
        new_subs = [sub for sub in self._subs if sub[0] != sub_id]
        print(new_subs)
        self._subs = new_subs

    def close(self):
        self._ser.close()

    def get_communication_history(self):
        return self._communication_history.decode('ascii')

    def at_version(self):
        return ELM_VERSION

    def st_version(self):
        return STN_VERSION

    def st_device_version(self):
        return STN_DEVICE_VERSION


if __name__ == "__main__":
    from vehicle import Vehicle
    from serial_device_interface import SerialDeviceInterface

    try:
        vehicle = Vehicle()
        vehicle.start_engine()
        sdi = SerialDeviceInterface()
        sdi.open()

        stn = STN(sdi.device_one, vehicle)

        stn.start()
        print('STN has started working')
        stn.join()

    except KeyboardInterrupt:
        print('Stopping stn chip reads')
        stn.stop_reading()

        stn.join()
    finally:
        # stop
        stn.close()
        sdi.close()

