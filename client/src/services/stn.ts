import axios from 'axios';

const baseUrl = 'http://localhost:5000';

export const getStnCommunication = async () => {
  const response = await axios.get<string>(`${baseUrl}/stn/communication`);

  const { data } = response;
  return data;
}

export default {
  getStnCommunication,
}
