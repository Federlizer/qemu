#!/usr/bin/python3.9

import subprocess

kernel_path = '/home/federlizer/code/federlizer/qemu/qemu-rpi-kernel/kernel-qemu-5.4.51-buster'
image_path = '/home/federlizer/code/federlizer/qemu/2021-03-04-raspios-buster-armhf-lite.img'
dtb_path = '/home/federlizer/code/federlizer/qemu/qemu-rpi-kernel/versatile-pb-buster-5.4.51.dtb'

def main():
    qemu_cli = [
        'qemu-system-arm',
        '-M', 'versatilepb',
        '-cpu', 'arm1176',
        '-m', '256',
        '-drive', 'file={},if=none,index=0,media=disk,format=raw,id=disk0'.format(image_path),
        '-device', 'virtio-blk-pci,drive=disk0,disable-modern=on,disable-legacy=off',
        '-net', 'user,hostfwd=tcp::5022-:22',
        '-dtb', '{}'.format(dtb_path),
        '-kernel', '{}'.format(kernel_path),
        '-append', 'root=/dev/vda2 panic=1',
        '-no-reboot',
        '-nographic',
    ]

    qemu_process = subprocess.Popen(
        qemu_cli,
        #stdin=subprocess.PIPE,
        #stdout=subprocess.PIPE,
        #stderr=subprocess.PIPE,
    )


    qemu_process.wait()


if __name__ == "__main__":
    main()