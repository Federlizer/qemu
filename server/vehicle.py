import re

RPM_QUERY   = 0x0C
SPEED_QUERY = 0x0D

class Vehicle(object):
    pid_pattern = re.compile('^([A-Z0-9]*)#([A-Z0-9]*)$') # pattern for PIDs
    engine_running = False
    values = {
        "RPM": 0,
        "SPEED": 120,
    }

    def __init__(self):
        pass

    def get_value(self, name):
        if name not in self.values:
            raise ValueError('{} is not a valid value'.format(name))

        return self.values[name]

    def set_value(self, name, value):
        if name not in self.values:
            raise ValueError('{} is not a valid value'.format(name))
        
        self.values[name] = value

    def start_engine(self):
        if self.engine_running:
            return

        self.values['RPM'] = 1000
        self.engine_running = True

    def stop_engine(self):
        if not self.engine_running:
            return

        self.values['RPM'] = 0
        self.engine_running = False

    def pid_query(self, message):
        """
        Implemented only for RPM and SPEED for simplicity.
        PID is constructed of 11bit header and 7 bytes body ( almost 10 bytes in total, not that it matters )
        """
        match = self.pid_pattern.match(message)

        if not match:
            return 'ERROR'

        _, pid = message.split('#')

        pid = bytes.fromhex(pid)

        message_len = pid[0]
        mode = pid[1]
        pid_query = pid[2:message_len+1]
        num_query = int.from_bytes(pid_query, byteorder='big')

        if num_query == RPM_QUERY:
            # figure response out
            service = '4{}'.format(mode)
            val = format(self.values['RPM'], '04X')

            response_data = '{}{}'.format(service, val)
            response_len = int(len(response_data) / 2)

            # construct response
            res = '7E8#{:02d}{}'.format(response_len, response_data)
            res = format(res, '<018')

        elif num_query == SPEED_QUERY:
            # figure response out
            service = '4{}'.format(mode)
            val = format(self.values['SPEED'], '02X')

            response_data = '{}{}'.format(service, val)
            response_len = int(len(response_data) / 2)

            # construct response
            res = '7E8#{:02d}{}'.format(response_len, response_data)
            res = format(res, '<018')

        else:
            return 'NO DATA'

        return res


# some test
if __name__ == '__main__':
    vehicle = Vehicle()
    vehicle.start_engine()

    res = vehicle.pid_query('7DF#02010D00000000')
    print(res)
