from flask import Flask, request, Response
from flask_socketio import SocketIO, emit
from flask_cors import CORS

from vehicle import Vehicle
from serial_device_interface import SerialDeviceInterface
from stn import STN

# flask setup
app = Flask(__name__)
cors = CORS(app)
socketio = SocketIO(app, cors_allowed_origins="*")


@app.route('/vehicle/engine_status')
def get_vehicle_engine_status():
    res = {
        "engine_running": vehicle.engine_running,
    }
    return res


@app.route('/vehicle/values', methods=['GET'])
def get_vehicle_values():
    res = vehicle.values.copy()
    return res


@app.route('/vehicle/values', methods=['POST'])
def set_vehicle_values():
    for name, value in request.json.items():
        vehicle.set_value(name, value)

    ret = {}
    for name in request.json.keys():
        ret[name] = vehicle.get_value(name)

    return ret


@app.route('/stn/communication', methods=['GET'])
def get_stn_communication():
    comm = stn.get_communication_history()
    return comm


@socketio.on('message')
def handle_message(data):
    print('Received data: {}'.format(data))
    emit('stn communication', data)


if __name__ == "__main__":
    def cb(comm):
        print(comm)
        socketio.emit('stn communication', comm)
        print('Sent emit')

    try:
        # internal setup
        vehicle = Vehicle()
        serial_device_iface = SerialDeviceInterface()
        serial_device_iface.open()

        stn = STN(serial_device_iface.device_one, vehicle)

        lock_acquired = stn.lock.acquire()
        if not lock_acquired:
            raise Exception("Did not acquire lock")

        stn.start()
        sub_id = stn.subscribe(cb)
        print('STN chip started.')
        stn.lock.release()

        # start flask server
        socketio.run(app)

    except KeyboardInterrupt:
        stn.unsubscribe(sub_id)
        stn.stop_reading()
        stn.join()

    finally:
        stn.close()
        serial_device_iface.close()
