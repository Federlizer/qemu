import axios from 'axios';

const baseUrl = 'http://localhost:5000'

interface ApiVehicleValues {
  [valueName: string]: number;
}

export const getVehicleValues = async () => {
  const response = await axios.get<ApiVehicleValues>(`${baseUrl}/vehicle/values`)

  const { data } = response;
  return data;
}

export const setVehicleValues = async (values: {[valueName: string]: number}) => {
  const response = await axios.post(`${baseUrl}/vehicle/values`, values);

  if (response.status !== 200) {
    throw new Error(`Something went wrong with the request, got ${response.status}`);
  }

  const { data } = response;

  return data;
}

export default {
  getVehicleValues,
  setVehicleValues,
}
