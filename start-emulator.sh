#!/bin/bash

QEMU_PATH="/usr/bin"

VERSATILE_IMAGE_FILE="/home/federlizer/code/federlizer/qemu/big_files/autopi_core_tmu_270520_versatilepb.img"
VERSATILE_DTB_FILE="/home/federlizer/code/federlizer/qemu/big_files/qemu-rpi-kernel-master/versatile-pb-buster-5.4.51.dtb"
VERSATILE_KERNEL_FILE="/home/federlizer/code/federlizer/qemu/big_files/qemu-rpi-kernel-master/kernel-qemu-5.4.51-buster"

RASPI_IMAGE_FILE="/home/federlizer/code/federlizer/qemu/big_files/autopi_core_tmu_270520.img"

PI_ZERO_DTB_FILE="/home/federlizer/code/federlizer/qemu/big_files/dtbs/bcm2708-rpi-zero-w.dtb"
PI_ZERO_KERNEL_FILE="/home/federlizer/code/federlizer/qemu/big_files/kernels/kernel.img"

PI_3_DTB_FILE="/home/federlizer/code/federlizer/qemu/big_files/dtbs/bcm2710-rpi-3-b.dtb"
PI_3_KERNEL_FILE="/home/federlizer/code/federlizer/qemu/big_files/kernels/kernel7.img"

SERIAL_PATH="/home/federlizer/code/federlizer/qemu/emulator/ttyone"

$QEMU_PATH/qemu-system-arm \
  -M raspi0 \
  -sd $RASPI_IMAGE_FILE \
  -dtb $PI_ZERO_DTB_FILE \
  -kernel $PI_ZERO_KERNEL_FILE \
  -append 'root=/dev/mmcblk0p2 console=tty1 panic=1' \
  -no-reboot

<<COMMENT
$QEMU_PATH/qemu-system-arm \
  -M versatilepb \
  -cpu arm1176 \
  -m 256 \
  -drive "file=$VERSATILE_IMAGE_FILE,if=none,index=0,media=disk,format=raw,id=disk0" \
  -device "virtio-blk-pci,drive=disk0,disable-modern=on,disable-legacy=off" \
  -dtb $VERSATILE_DTB_FILE \
  -kernel $VERSATILE_KERNEL_FILE \
  -chardev "tty,path=$SERIAL_PATH,id=myserial0" \
  -serial "chardev:myserial0" \
  -append 'root=/dev/vda2 console=tty1 panic=1' \
  -no-reboot


$QEMU_PATH/qemu-system-aarch64 \
  -M raspi0 \
  -kernel $PI_ZERO_KERNEL_FILE \
  -dtb $PI_ZERO_DTB_FILE \
  -sd $RASPI_IMAGE_FILE \
  -append "root=/dev/mmcblk0p2 console=tty1 panic=1" \
  -no-reboot


$QEMU_PATH/qemu-system-aarch64 \
  -M raspi3 \
  -kernel $PI_3_KERNEL_FILE \
  -dtb $PI_3_DTB_FILE \
  -sd $RASPI_IMAGE_FILE \
  -append "root=/dev/mmcblk0p2 console=tty1 panic=1" \
  -no-reboot


COMMENT
